# Branching Entropy (BE) & Cohesion Probability (CP)

노이즈가 많은 임의의 텍스트(트위터 텍스트 또는 댓글 텍스트)에서 의미있는 단위로 
단어를 잘라내거나 신조어를 찾고자 할 때, 사용할 수 있는 방법론

* Branching Entropy (BE) - 단어 내부에서는 불확실성(uncertainty), 엔트로피(entropy)가 줄어들고, 단어경계에서는 증가하는 현상을 모델링한 것

* Cohesion Probability (CP) -  연속된 글자의 연관성이 높을 수록 단어일 가능성이 높다는 가정

## To-Do

* 글자를 자모로 분할해서 자모 단위로 단어를 찾는 알고리듬 만들기.
* 사전을 빌드해 보기 - 미등록어 찾기.
* 단어의 어근을 추정해 보기.
* 성능 측정하기.
* 위키 텍스트 전체를 시험해 보기.
* 트위터 텍스트 전체를 시험해 보기.
* 사용성을 높이기 - 최소한의 모듈 Pure Pyhton 모듈 만들기.
* 일본어에 적용해 보기.
* 에스페란토에 적용해 보기.

## 참고 자료

* https://ratsgo.github.io/from%20frequency%20to%20semantics/2017/05/06/BranchingEntropy/

원래 저자 - 서울대 박사과정 김현중의 논문 및 코드

* https://github.com/lovit/soy
