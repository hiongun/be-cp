#!/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import BranchingEntropy as BE
import MaxScoreTokenizer as MST

reviews = ["우리는 민족 중흥의 역사적 사명을 띠고 이땅에 태어났다.",
    "우리와 같은 사람들은 중국인이 아닌 역사적 사명과 함계 태어난 사람들이다.",
    "너희는 왜 민족의 반역자들과 역사적 중흥을 같이 생각하느냐?" ]

branching = BE.BranchingEntropy()
branching.train(reviews)
tokenizer = MST.MaxScoreTokenizer(scores=branching.get_all_branching_entropies())
tokenized_reviews = [tokenizer.tokenize(review) for review in reviews]

for review in tokenized_reviews:
    for word in review:
        print("%s" % (word)),
    print("")

